package com.example.portfolio_project.repository;


import com.example.portfolio_project.entities.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ReviewRepository extends JpaRepository<Review, Integer> {
}
