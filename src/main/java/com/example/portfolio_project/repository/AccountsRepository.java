package com.example.portfolio_project.repository;

import com.example.portfolio_project.entities.Accounts;

import org.springframework.data.repository.CrudRepository;

public interface AccountsRepository extends CrudRepository<Accounts, Integer> {

    Accounts findByEmailAndPassword(String email, String password);
}
