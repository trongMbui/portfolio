package com.example.portfolio_project.entities;

import javax.persistence.*;

@Entity
@Table(name = "cleaners")
public class Cleaners {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer clId;

    @Column(nullable = false, unique = true, length = 45)
    private String email;

    @Column(length = 15, nullable = false)
    private String password;

    @Column(length = 45, nullable = false, name = "first_name")
    private String firstName;

    @Column(length = 45, nullable = false, name = "last_name")
    private String lastName;

    @Column(length = 45, nullable = false )
    private String address;

    @Column(length = 45, nullable = false )
    private Integer telephoneNr;

    @Column(length = 45, nullable = false)
    private String role;


    public Integer getClId() {
        return clId;
    }

    public void setClId(Integer clId) {
        this.clId = clId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getTelephoneNr() {
        return telephoneNr;
    }

    public void setTelephoneNr(Integer telephoneNr) {
        this.telephoneNr = telephoneNr;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Cleaners{" +
                "clId=" + clId +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", telephoneNr=" + telephoneNr +
                ", role='" + role + '\'' +
                '}';
    }
}
