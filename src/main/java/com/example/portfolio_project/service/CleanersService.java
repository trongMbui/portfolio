package com.example.portfolio_project.service;

import com.example.portfolio_project.entities.Accounts;
import com.example.portfolio_project.entities.Cleaners;
import com.example.portfolio_project.repository.CleanersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CleanersService {

    @Autowired
    private CleanersRepository repo;


    public Cleaners validate(String email, String password) {

        Cleaners user = repo.findByEmailAndPassword(email, password);
        return user;
    }
}
