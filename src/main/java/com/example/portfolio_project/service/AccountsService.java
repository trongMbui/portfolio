package com.example.portfolio_project.service;

import com.example.portfolio_project.entities.Accounts;

import com.example.portfolio_project.entities.Booking;
import com.example.portfolio_project.repository.AccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountsService {

    @Autowired private AccountsRepository repo;

    public List<Accounts> listAll(){
        return (List<Accounts>) repo.findAll();
    }

    public void register(Accounts accounts){
        repo.save(accounts);
    }

    public Accounts validate(String email, String password) {
        Accounts user = repo.findByEmailAndPassword(email, password);
        return user;

    }
    public Accounts get(Integer id) throws UserNotFoundException {

        Optional<Accounts> result = repo.findById(id);

        if(result.isPresent()){
            return result.get();

        }
        throw
                new UserNotFoundException("Could not find Accounts with id" + id);
    }
}
